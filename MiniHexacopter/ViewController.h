//
//  ViewController.h
//  MiniHexacopter
//
//  Created by Oxygen on 15/4/12.
//  Copyright (c) 2015年 Oxygen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "PNChart/PNChart.h"

@interface ViewController : UIViewController <UITextFieldDelegate, CBPeripheralDelegate, CBCentralManagerDelegate>
{
	CBPeripheral *periph;
	CBCentralManager *manager;
	CBCharacteristic *transport;
	CBCharacteristic *controller;

	PNBarChart *barChart;
	PNLineChart *lineChart;

	NSMutableArray *arrayYaw;
	NSMutableArray *arrayRoll;
	NSMutableArray *arrayPitch;

	PNLineChartData *dataYaw;
	PNLineChartData *dataRoll;
	PNLineChartData *dataPitch;
}

@property (weak, nonatomic) IBOutlet UILabel *labelStatus;
@property (weak, nonatomic) IBOutlet UILabel *labelYawAngle;
@property (weak, nonatomic) IBOutlet UILabel *labelRollAngle;
@property (weak, nonatomic) IBOutlet UILabel *labelPitchAngle;

@property (weak, nonatomic) IBOutlet UISlider *sliderRoll;
@property (weak, nonatomic) IBOutlet UISlider *sliderThrottle;

@property (weak, nonatomic) IBOutlet UIButton *buttonApply;
@property (weak, nonatomic) IBOutlet UIButton *buttonReset;
@property (weak, nonatomic) IBOutlet UIButton *buttonRescan;

@property (weak, nonatomic) IBOutlet UIButton *buttonStart;
@property (weak, nonatomic) IBOutlet UIButton *buttonCalibrate;

@property (weak, nonatomic) IBOutlet UITextField *textYawP;
@property (weak, nonatomic) IBOutlet UITextField *textYawI;
@property (weak, nonatomic) IBOutlet UITextField *textYawD;

@property (weak, nonatomic) IBOutlet UITextField *textRollP;
@property (weak, nonatomic) IBOutlet UITextField *textRollI;
@property (weak, nonatomic) IBOutlet UITextField *textRollD;

@property (weak, nonatomic) IBOutlet UITextField *textPitchP;
@property (weak, nonatomic) IBOutlet UITextField *textPitchI;
@property (weak, nonatomic) IBOutlet UITextField *textPitchD;

@property (weak, nonatomic) IBOutlet UITextField *textOutterRoll;
@property (weak, nonatomic) IBOutlet UITextField *textOutterPitch;

- (IBAction)buttonClearClicked:(id)sender;
- (IBAction)buttonResetClicked:(id)sender;
- (IBAction)buttonRescanClicked:(id)sender;

- (IBAction)sliderRollChanged:(id)sender;
- (IBAction)sliderThrottleChanged:(id)sender;

- (IBAction)buttonStartClicked:(id)sender;
- (IBAction)buttonCalibrateClicked:(id)sender;

- (IBAction)buttonApplyClicked:(id)sender;

@end
