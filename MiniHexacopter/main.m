//
//  main.m
//  MiniHexacopter
//
//  Created by Oxygen on 15/4/12.
//  Copyright (c) 2015年 Oxygen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
