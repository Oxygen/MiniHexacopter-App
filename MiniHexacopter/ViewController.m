//
//  ViewController.m
//  MiniHexacopter
//
//  Created by Oxygen on 15/4/12.
//  Copyright (c) 2015年 Oxygen. All rights reserved.
//

#import "ViewController.h"

#define P_MAGIC					0xA5

#define CMD_YAW					0x01
#define CMD_ROLL				0x02
#define CMD_PITCH				0x03
#define CMD_THROTTLE			0x04

#define CMD_KILL				0xC0
#define CMD_START				0xC1

#define CMD_STOP_CALIB			0xC2
#define CMD_START_CALIB			0xC3

#define CMD_SET_PID_INNER		0xD0
#define CMD_SET_PID_OUTTER		0xD1

#define CMDR_RUNTIME			0xA0

#define ARRAY_POINTS			256

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
	[super viewDidLoad];

	manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
	barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(700.0f, 500.0f, 250.0f, 250.0f)];
	lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(70.0f, 80.0f, 800.0f, 400.0f)];

	arrayYaw = [[NSMutableArray alloc] init];
	arrayRoll = [[NSMutableArray alloc] init];
	arrayPitch = [[NSMutableArray alloc] init];

	dataYaw = [[PNLineChartData alloc] init];
	dataRoll = [[PNLineChartData alloc] init];
	dataPitch = [[PNLineChartData alloc] init];

	self.textYawP.delegate = self;
	self.textYawI.delegate = self;
	self.textYawD.delegate = self;
	self.textRollP.delegate = self;
	self.textRollI.delegate = self;
	self.textRollD.delegate = self;
	self.textPitchP.delegate = self;
	self.textPitchI.delegate = self;
	self.textPitchD.delegate = self;
	self.textOutterRoll.delegate = self;
	self.textOutterPitch.delegate = self;
	self.sliderThrottle.transform = CGAffineTransformMakeRotation(-M_PI_2);

	for (int i = 0; i < ARRAY_POINTS; i++)
	{
		[arrayYaw addObject:[NSNumber numberWithInt:0]];
		[arrayRoll addObject:[NSNumber numberWithInt:0]];
		[arrayPitch addObject:[NSNumber numberWithInt:0]];
	}

	dataYaw.alpha = 1.0f;
	dataYaw.color = PNRed;
	dataYaw.lineWidth = 1.0f;
	dataYaw.dataTitle = @"Yaw";
	dataYaw.itemCount = ARRAY_POINTS;
	dataYaw.inflexionPointStyle = PNLineChartPointStyleNone;

	dataRoll.alpha = 1.0f;
	dataRoll.color = PNFreshGreen;
	dataRoll.lineWidth = 1.0f;
	dataRoll.dataTitle = @"Roll";
	dataRoll.itemCount = ARRAY_POINTS;
	dataRoll.inflexionPointStyle = PNLineChartPointStyleNone;

	dataPitch.alpha = 1.0f;
	dataPitch.color = PNBlue;
	dataPitch.lineWidth = 1.0f;
	dataPitch.dataTitle = @"Pitch";
	dataPitch.itemCount = ARRAY_POINTS;
	dataPitch.inflexionPointStyle = PNLineChartPointStyleNone;

	__weak NSArray *yaw = arrayYaw;
	__weak NSArray *roll = arrayRoll;
	__weak NSArray *pitch = arrayPitch;

	dataYaw.getData = ^(NSUInteger index) { return [PNLineChartDataItem dataItemWithY:[[yaw objectAtIndex:index] integerValue]]; };
	dataRoll.getData = ^(NSUInteger index) { return [PNLineChartDataItem dataItemWithY:[[roll objectAtIndex:index] integerValue]]; };
	dataPitch.getData = ^(NSUInteger index) { return [PNLineChartDataItem dataItemWithY:[[pitch objectAtIndex:index] integerValue]]; };

	barChart.yMinValue = 0.0f;
	barChart.yMaxValue = 65535.0f;
	barChart.labelMarginTop = 5.0f;
	barChart.backgroundColor = [UIColor clearColor];
	barChart.yLabelFormatter = ^(CGFloat yValue) { return [NSString stringWithFormat:@"%1.f", yValue]; };

	lineChart.showLabel = YES;
	lineChart.chartData = @[dataYaw, dataRoll, dataPitch];
	lineChart.legendStyle = PNLegendItemStyleStacked;
	lineChart.yLabelFormat = @"%1.1f";
	lineChart.yFixedValueMax = 1800.0f;
	lineChart.yFixedValueMin = -1800.0f;
	lineChart.showCoordinateAxis = YES;

	UIView *legend = [lineChart getLegendWithMaxWidth:100.0f];

	[legend setFrame:CGRectMake(80.0f, 10.0f, 120.0f, 100.0f)];
	[lineChart addSubview:legend];
	[self.view addSubview:barChart];
	[self.view addSubview:lineChart];

	[barChart setXLabels:@[@"F", @"B", @"LF", @"LB", @"RF", @"RB"]];
	[barChart setYValues:@[@0, @0, @0, @0, @0, @0]];
	[barChart setStrokeColors:@[PNGreen, PNGreen, PNGreen, PNGreen, PNGreen, PNGreen]];
	[barChart strokeChart];
	[lineChart strokeChart];
	[lineChart updateChartData:@[dataYaw, dataRoll, dataPitch]];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}

- (IBAction)buttonClearClicked:(id)sender
{
	[arrayYaw removeAllObjects];
	[arrayRoll removeAllObjects];
	[arrayPitch removeAllObjects];

	for (int i = 0; i < ARRAY_POINTS; i++)
	{
		[arrayYaw addObject:[NSNumber numberWithInt:0]];
		[arrayRoll addObject:[NSNumber numberWithInt:0]];
		[arrayPitch addObject:[NSNumber numberWithInt:0]];
	}

	[barChart updateChartData:@[@0, @0, @0, @0, @0, @0]];
	[lineChart updateChartData:@[dataYaw, dataRoll, dataPitch]];
}

- (IBAction)buttonResetClicked:(id)sender
{
	char data[2] = { P_MAGIC, CMD_KILL };

	if (periph && transport && controller)
	{
		self.buttonStart.enabled = YES;

		self.sliderRoll.value = 0;
		self.sliderThrottle.value = 0;

		[periph writeValue:[[NSData alloc] initWithBytesNoCopy:data length:2 freeWhenDone:NO]
		 forCharacteristic:transport
					  type:CBCharacteristicWriteWithoutResponse];
	}
}

- (IBAction)buttonRescanClicked:(id)sender
{
	periph = nil;
	transport = nil;
	controller = nil;

	self.labelStatus.text = @"Searching ...";
	[manager scanForPeripheralsWithServices:nil options:nil];
}

- (IBAction)sliderRollChanged:(id)sender
{
	char data[4] = { P_MAGIC, CMD_PITCH, 0x00, 0x00 };

	if (periph && transport && controller)
	{
		*(int16_t *)(&(data[2])) = (int16_t)self.sliderRoll.value;

		[periph writeValue:[[NSData alloc] initWithBytesNoCopy:data length:4 freeWhenDone:NO]
		 forCharacteristic:transport
					  type:CBCharacteristicWriteWithoutResponse];
	}
}

- (IBAction)sliderThrottleChanged:(id)sender
{
	char data[4] = { P_MAGIC, CMD_THROTTLE, 0x00, 0x00 };

	if (periph && transport && controller)
	{
		*(uint16_t *)(&(data[2])) = (uint16_t)self.sliderThrottle.value;

		[periph writeValue:[[NSData alloc] initWithBytesNoCopy:data length:4 freeWhenDone:NO]
		 forCharacteristic:transport
					  type:CBCharacteristicWriteWithoutResponse];
	}
}

- (IBAction)buttonStartClicked:(id)sender
{
	char data[2] = { P_MAGIC, CMD_START };

	if (periph && transport && controller)
	{
		self.buttonStart.enabled = NO;

		self.sliderRoll.value = 0;
		self.sliderThrottle.value = 0;

		[periph writeValue:[[NSData alloc] initWithBytesNoCopy:data length:2 freeWhenDone:NO]
		 forCharacteristic:transport
					  type:CBCharacteristicWriteWithoutResponse];
	}
}

- (IBAction)buttonCalibrateClicked:(id)sender
{
	char data[2] = { P_MAGIC, 0 };

	if (periph && transport && controller)
	{
		if ([self.buttonCalibrate.titleLabel.text isEqualToString:@"Finish"])
		{
			data[1] = CMD_STOP_CALIB;
			[self.buttonCalibrate setTitle:@"Calibrate" forState:UIControlStateNormal];
		}
		else
		{
			data[1] = CMD_START_CALIB;
			[self.buttonCalibrate setTitle:@"Finish" forState:UIControlStateNormal];
		}

		[periph writeValue:[[NSData alloc] initWithBytesNoCopy:data length:2 freeWhenDone:NO]
		 forCharacteristic:transport
					  type:CBCharacteristicWriteWithoutResponse];
	}
}

- (IBAction)buttonApplyClicked:(id)sender
{
	char data[20] = { P_MAGIC };

	if (periph && transport && controller)
	{
		data[1] = CMD_SET_PID_INNER;
		*(short *)(&(data[ 2])) = [self.textYawP.text intValue];
		*(short *)(&(data[ 4])) = [self.textYawI.text intValue];
		*(short *)(&(data[ 6])) = [self.textYawD.text intValue];
		*(short *)(&(data[ 8])) = [self.textRollP.text intValue];
		*(short *)(&(data[10])) = [self.textRollI.text intValue];
		*(short *)(&(data[12])) = [self.textRollD.text intValue];
		*(short *)(&(data[14])) = [self.textPitchP.text intValue];
		*(short *)(&(data[16])) = [self.textPitchI.text intValue];
		*(short *)(&(data[18])) = [self.textPitchD.text intValue];

		[periph writeValue:[[NSData alloc] initWithBytesNoCopy:data length:20 freeWhenDone:NO]
		 forCharacteristic:transport
					  type:CBCharacteristicWriteWithoutResponse];

		data[1] = CMD_SET_PID_OUTTER;
		*(short *)(&(data[ 2])) = [self.textOutterRoll.text intValue];
		*(short *)(&(data[ 4])) = [self.textOutterPitch.text intValue];

		[periph writeValue:[[NSData alloc] initWithBytesNoCopy:data length:6 freeWhenDone:NO]
		 forCharacteristic:transport
					  type:CBCharacteristicWriteWithoutResponse];
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	self.view.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	self.view.frame = CGRectMake(0.0, -350, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
	switch (central.state)
	{
		case CBCentralManagerStatePoweredOn:
		{
			self.labelStatus.text = @"Searching ...";
			[central scanForPeripheralsWithServices:nil options:nil];
			break;
		}

		case CBCentralManagerStatePoweredOff:
		{
			self.labelStatus.text = @"Switched Off";
			break;
		}

		case CBCentralManagerStateUnauthorized:
		{
			self.labelStatus.text = @"Unauthorized";
			break;
		}

		default:
		{
			self.labelStatus.text = @"<Unknown State>";
			break;
		}
	}
}

- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
	 advertisementData:(NSDictionary *)advertisementData
				  RSSI:(NSNumber *)RSSI
{
	if ([peripheral.name isEqualToString:@"Hexacopter"])
	{
		periph = peripheral;

		self.labelStatus.text = @"Connecting ...";
		self.buttonApply.enabled = NO;
		self.buttonReset.enabled = NO;
		self.buttonStart.enabled = NO;
		self.buttonRescan.enabled = YES;
		self.buttonCalibrate.enabled = NO;

		self.sliderRoll.enabled = NO;
		self.sliderThrottle.enabled = NO;

		self.textYawP.enabled = NO;
		self.textYawI.enabled = NO;
		self.textYawD.enabled = NO;
		self.textRollP.enabled = NO;
		self.textRollI.enabled = NO;
		self.textRollD.enabled = NO;
		self.textPitchP.enabled = NO;
		self.textPitchI.enabled = NO;
		self.textPitchD.enabled = NO;
		self.textOutterRoll.enabled = NO;
		self.textOutterPitch.enabled = NO;

		[central stopScan];
		[manager connectPeripheral:peripheral options:nil];
	}
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
	peripheral.delegate = self;
	self.labelStatus.text = @"Enumerating Services ...";

	[peripheral discoverServices:nil];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
	periph = nil;
	transport = nil;
	controller = nil;

	self.labelStatus.text = @"Disconnected";
	self.buttonApply.enabled = NO;
	self.buttonReset.enabled = NO;
	self.buttonStart.enabled = NO;
	self.buttonRescan.enabled = YES;
	self.buttonCalibrate.enabled = NO;

	self.sliderRoll.enabled = NO;
	self.sliderThrottle.enabled = NO;

	self.textYawP.enabled = NO;
	self.textYawI.enabled = NO;
	self.textYawD.enabled = NO;
	self.textRollP.enabled = NO;
	self.textRollI.enabled = NO;
	self.textRollD.enabled = NO;
	self.textPitchP.enabled = NO;
	self.textPitchI.enabled = NO;
	self.textPitchD.enabled = NO;
	self.textOutterRoll.enabled = NO;
	self.textOutterPitch.enabled = NO;

	[manager scanForPeripheralsWithServices:nil options:nil];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
	periph = nil;
	transport = nil;
	controller = nil;

	self.labelStatus.text = @"Connect Failed";
	self.buttonApply.enabled = NO;
	self.buttonReset.enabled = NO;
	self.buttonStart.enabled = NO;
	self.buttonRescan.enabled = YES;
	self.buttonCalibrate.enabled = NO;

	self.sliderRoll.enabled = NO;
	self.sliderThrottle.enabled = NO;

	self.textYawP.enabled = NO;
	self.textYawI.enabled = NO;
	self.textYawD.enabled = NO;
	self.textRollP.enabled = NO;
	self.textRollI.enabled = NO;
	self.textRollD.enabled = NO;
	self.textPitchP.enabled = NO;
	self.textPitchI.enabled = NO;
	self.textPitchD.enabled = NO;
	self.textOutterRoll.enabled = NO;
	self.textOutterPitch.enabled = NO;

	[manager scanForPeripheralsWithServices:nil options:nil];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
	if (peripheral.services.count != 1)
	{
		self.labelStatus.text = @"Invalid BLE Device";
		[manager cancelPeripheralConnection:periph];
	}
	else
	{
		CBService *service = (CBService *)peripheral.services.firstObject;

		if (*(const uint16_t *)service.UUID.data.bytes != 0xF0FA)
		{
			self.labelStatus.text = @"Invalid BLE Device";
			[manager cancelPeripheralConnection:periph];
		}
		else
		{
			self.labelStatus.text = @"Enumerating Characteristics ...";
			[peripheral discoverCharacteristics:nil forService:service];
		}
	}
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
	if (service.characteristics.count != 2)
	{
		self.labelStatus.text = @"Invalid BLE Device";
		[manager cancelPeripheralConnection:periph];
	}
	else
	{
		transport = (CBCharacteristic *)[service.characteristics objectAtIndex:0];
		controller = (CBCharacteristic *)[service.characteristics objectAtIndex:1];

		if (*(uint16_t *)transport.UUID.data.bytes != 0xF1FA ||
			*(uint16_t *)controller.UUID.data.bytes != 0xF2FA)
		{
			self.labelStatus.text = @"Invalid BLE Device";
			[manager cancelPeripheralConnection:periph];
		}
		else
		{
			self.labelStatus.text = @"Connected";
			self.buttonApply.enabled = YES;
			self.buttonReset.enabled = YES;
			self.buttonStart.enabled = YES;
			self.buttonRescan.enabled = NO;
			self.buttonCalibrate.enabled = YES;

			self.sliderRoll.enabled = YES;
			self.sliderThrottle.enabled = YES;

			self.textYawP.enabled = YES;
			self.textYawI.enabled = YES;
			self.textYawD.enabled = YES;
			self.textRollP.enabled = YES;
			self.textRollI.enabled = YES;
			self.textRollD.enabled = YES;
			self.textPitchP.enabled = YES;
			self.textPitchI.enabled = YES;
			self.textPitchD.enabled = YES;
			self.textOutterRoll.enabled = YES;
			self.textOutterPitch.enabled = YES;

			[peripheral setNotifyValue:YES forCharacteristic:transport];
			[peripheral setNotifyValue:YES forCharacteristic:controller];
		}
	}
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
	if (characteristic == transport)
	{
		const uint8_t *data = characteristic.value.bytes;

		if (characteristic.value.length != 20 || *data++ != P_MAGIC)
			return;

		switch (*data++)
		{
			case CMDR_RUNTIME:
			{
				int16_t yaw   = *(int16_t *)data; data += 2;
				int16_t roll  = *(int16_t *)data; data += 2;
				int16_t pitch = *(int16_t *)data; data += 2;

				uint16_t f    = *(uint16_t *)data; data += 2;
				uint16_t b    = *(uint16_t *)data; data += 2;
				uint16_t lf   = *(uint16_t *)data; data += 2;
				uint16_t lb   = *(uint16_t *)data; data += 2;
				uint16_t rf   = *(uint16_t *)data; data += 2;
				uint16_t rb   = *(uint16_t *)data; data += 2;

				if (yaw > 1800)
					yaw -= 3600;

				self.labelYawAngle.text = [NSString stringWithFormat:@"%1.1f", yaw / 10.0];
				self.labelRollAngle.text = [NSString stringWithFormat:@"%1.1f", roll / 10.0];
				self.labelPitchAngle.text = [NSString stringWithFormat:@"%1.1f", pitch / 10.0];

				[arrayYaw removeObjectAtIndex:0];
				[arrayRoll removeObjectAtIndex:0];
				[arrayPitch removeObjectAtIndex:0];

				[arrayYaw addObject:@(yaw)];
				[arrayRoll addObject:@(roll)];
				[arrayPitch addObject:@(pitch)];

				[barChart updateChartData:@[@(f), @(b), @(lf), @(lb), @(rf), @(rb)]];
				[lineChart updateChartData:@[dataYaw, dataRoll, dataPitch]];
				break;
			}
		}
	}
	else if (characteristic == controller)
	{
		NSLog(@"error: %@, command: %@", error, characteristic.value);
	}
}

@end
